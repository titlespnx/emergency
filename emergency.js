const MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

const line = require('@line/bot-sdk')
const express = require('express')
const axios = require('axios').default
const dotenv = require('dotenv')
const { header } = require('express/lib/request');
const { text } = require('express');
const env = dotenv.config().parsed
const app = express()


const lineConfig = {
   channelAccessToken:env.ACCESS_TOKEN,
   channelSecret:env.SECRET_TOKEN
}

const client = new line.Client(lineConfig);

app.post('/webhook',line.middleware(lineConfig),async (req,res) => {
   try{
       const events = req.body.events
       console.log('events=>>>>',events)
       return events.length > 0 ? await events.map(item => handelEvent(item)) : res.status(200).send("OK") 
   }catch(error){
       res.status(500).end()
   }
});

const handelEvent = async (event) => {
  if (event.type !== 'message' ) {
    return Promise.resolve(null);
  }

  if (event.message.type == 'sticker' ) {
   return Promise.resolve(null);
 }
  const message = event.message.text;
  
   if(message == 'ข่มขู่ว่าจะทําร้าย / ทําร้าย' || message == 'กักขังหน่วงเหนี่ยว' || message == 'เสี่ยงถูกล่วงละเมิดทางเพศ' || message == 'ผู้คลุ้มคลั่งก่อให้เกิดเหตุร้าย' || message == 'มั่วสุมก่อให้เกิดเหตุร้าย'){
      // console.log(event.message.type);
      
      MongoClient.connect(url, function(err, db) {
         if (err) throw err;
         let dbo = db.db("mydb");
         let myobj = { id: event.source.userId , title: event.message.text};
         dbo.collection("emergency").insert(myobj, function(err, res) {
           if (err) throw err;
           console.log("insert section created!");
           db.close();
         });
       });   

        client.replyMessage(event.replyToken, {
         "type": `text`,
         "text": `กรุณาส่งที่อยู่ปัจจุบันของท่าน (Please send me your location!, I will report police officer for you.)` ,
         "quickReply": {
           "items": [
             {
               "type": "action",
               "action": {
                 "type": "location",
                 "label": "Send Location"
               }
             }
           ]
         }
       
      });
      
      }else if(event.message.type == 'location'){
         MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            let dbo = db.db("mydb");
            let myobj = { id: event.source.userId , address: event.message.address, latitude: event.message.latitude ,longitude: event.message.longitude };
            dbo.collection("emergency").insert(myobj, function(err, res) {
              if (err) throw err;
              console.log("insert location created!");
              db.close();
            });
          });   

         client.replyMessage(event.replyToken, {
         "type":"text",
         "text":"กรุณากรอกเบอร์มือถือที่ติดต่อได้"

      }); 
      }else if(message.startsWith('0') && event.type == 'message'){
         MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            let dbo = db.db("mydb");
            let myobj = { id: event.source.userId , Telephone: event.message.text};
            dbo.collection("emergency").insert(myobj, function(err, res) {
              if (err) throw err;
              console.log("insert contact created!");
              db.close();
            });
          });   
         client.replyMessage(event.replyToken, {
            "type":"text",
            "text":"ระบบกำลังแจ้งไปยังเจ้าหน้าที่ โปรดรอสักครู่"
         }); 
      } 
      else if (message !== 'ข่มขู่ว่าจะทําร้าย / ทําร้าย' || message !== 'กักขังหน่วงเหนี่ยว' || message !== 'เสี่ยงถูกล่วงละเมิดทางเพศ' 
      || message !== 'ผู้คลุ้มคลั่งก่อให้เกิดเหตุร้าย' || message !== 'มั่วสุมก่อให้เกิดเหตุร้าย' ) {
         client.replyMessage(event.replyToken, {
            "type":"text",
            "text":"กรุณาทำรายการใหม่อีกครั้ง"
         }); 
    
      }
 
}

app.listen(4000,() => {
   console.log('listening on 4000')
})
